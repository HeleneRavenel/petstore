app.controller('tagsController', function($scope,$http,NgTableParams,TagService,$route) {

    // load all tags
    TagService.getTags()
        .then(function mySuccess(response) {
            $scope.tableParams = new NgTableParams({
                count: response.data.length
            }, {
                counts:[],
                dataset: response.data
            });
        }, function myError(response) {
            swal("Error GET tags", response, "error");
        });

    //add a new tag
    $scope.addTag = function addTag() {
        if($scope.tagName==""){
            swal("Please provide values for tag name", "", "info");
        }
        else {
            TagService.addTag({
                id: null,
                name: $scope.tagName
            }).then(function mySuccess() {
                swal("Tag added", "", "success");
                $route.reload();
            }, function myError(response) {
                swal("Error POST tag", response, "error");
            });
        }
    };
});
