app.controller('petsController', function($scope,$rootScope,$http,NgTableParams,PetService,CategoryService,TagService,$route) {
    var urlBase="/api/pet";
    $scope.photoUrls = [];
    $rootScope.modalShown = false;

    // load all categories
    CategoryService.getCategories($scope.petCategory)
        .then(function mySuccess(response) {
            $scope.categories = response.data;
        }, function myError(response) {
            swal("Error GET categories", response, "error");
        });

    // load all tags
    TagService.getTags($scope.petTag1)
        .then(function mySuccess(response) {
            $scope.tags1 = response.data;
            $scope.items = _.clone(response.data);
        }, function myError(response) {
            swal("Error GET tags", response, "error");
        });

    // load all statuses
    $http({
        method: "GET",
        url: urlBase + '/statuses',
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json;charset=UTF-8'
        }
    }).then(function mySuccess(response) {
        $scope.statuses = response.data;
    }, function myError(response) {
        swal("Error GET statuses", response, "error");
    });

    // load all pets
    PetService.getPets()
        .then(function mySuccess(response) {
            $scope.tableParams = new NgTableParams({
                count: response.data.length
            }, {
                counts:[],
                dataset: response.data
            });
        }, function myError(response) {
            swal("Error GET pets", response, "error");
        });

    //add a new pet
    $scope.addPet = function addPet() {
        if($scope.petName==null||$scope.petCategory==null||$scope.petStatus==null){
            swal("Please provide values for name, category and status", "", "info");
        }
        else {
            PetService.addPet({
                id: null,
                category: $scope.petCategory,
                name: $scope.petName,
                photoUrls: $scope.photoUrls,
                tags: $scope.tags2,
                status: $scope.petStatus
            }).then(function mySuccess() {
                swal("Pet added", "", "success");
                $route.reload();
            }, function myError(response) {
                swal("Error POST pets", response, "error");
            });
        }
    };

    // remove a pet
    $scope.deletePet = function(id) {
        if ($rootScope.modalShown) {
            PetService.deletePet(id)
                .then(function mySuccess() {
                    swal("Pet removed", "", "success");
                    $rootScope.modalShown = false;
                    $route.reload();
                    $route.reload();
            }, function myError(response) {
                    swal("Error delete pet", response, "error");
            });
        }
    };

    // init
    $scope.selected1 = [];
    $scope.selected2 = [];

    $scope.tags2 = [];

    // select all in list1
    $scope.selectAll1 = function() {
        if ($scope.selected1.length>0) {
            $scope.selected1 = [];
        }
        else {
            for (var i = 0; i < $scope.tags1.length; i++) {
                $scope.selected1.push($scope.tags1[i].id);
            }
        }
    };

    // select all in list2
    $scope.selectAll2 = function() {
        if ($scope.selected2.length>0) {
            $scope.selected2 = [];
        }
        else {
            for (var i = 0; i < $scope.tags2.length; i++) {
                $scope.selected2.push($scope.tags2[i].id);
            }
        }
    };

    // move from list1 to list2
    $scope.tags1Totags2 = function() {
        for (var i = 0; i < $scope.selected1.length; i++) {
            var moveId = arrayObjectIndexOf($scope.items, $scope.selected1[i], "id");
            $scope.tags2.push($scope.items[moveId]);
            var delId = arrayObjectIndexOf($scope.tags1, $scope.selected1[i], "id");
            $scope.tags1.splice(delId,1);
        }
        reset();
    };

    // move from list2 to list1
    $scope.tags2Totags1 = function() {
        for (var i = 0; i < $scope.selected2.length; i++) {
            var moveId = arrayObjectIndexOf($scope.items, $scope.selected2[i], "id");
            $scope.tags1.push($scope.items[moveId]);
            var delId = arrayObjectIndexOf($scope.tags2, $scope.selected2[i], "id");
            $scope.tags2.splice(delId,1);
        }
        reset();
    };

    function arrayObjectIndexOf(myArray, searchTerm, property) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i][property] === searchTerm) {
                return i;
            }
        }
        return -1;
    };

    // select in list1
    $scope.select1 = function(id) {
        $scope.selected1.push(id);
    };

    // select in list2
    $scope.select2 = function(i) {
        $scope.selected2.push(i);
    };

    // reset init fields
    function reset(){
        $scope.selected1 = [];
        $scope.selected2 = [];
        $scope.toggle1 = 0;
        $scope.toggle = 0;
    };

    // add a photo
    $scope.addUrl = function() {
        $scope.photoUrls.push({
            ppId: null,
            photoUrl: $scope.petUrl
        });
    };

    // remove a photo
    $scope.removeUrl = function(id) {
        $scope.photoUrls.splice(id,1);
    };

    // get pet
    $scope.openModal = function(pet) {
        $scope.selectedPet = pet;
        $rootScope.modalShown = true;
    };

    // quit the modal
    $scope.close = function() {
        $rootScope.modalShown = false;
    };

    // get a list of all pet's tags
    $scope.getTagsList = function(petTags) {
        if ($rootScope.modalShown) {
            var tagsList = "";
            petTags.forEach(function (tag) {
                tagsList += tag.name + ", ";
            });
            if (tagsList.length > 0) {
                tagsList = tagsList.substring(0, tagsList.length - 2);
            }
            return tagsList;
        }
    };
});
