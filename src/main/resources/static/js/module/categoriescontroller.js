app.controller('categoriesController', function($scope,$http,NgTableParams,CategoryService,$route) {

    // load all categories
    CategoryService.getCategories()
        .then(function mySuccess(response) {
            $scope.tableParams = new NgTableParams({
                count:response.data.length
            }, {
                counts: [],
                dataset: response.data
            });
        }, function myError(response) {
            swal("Error GET categories", response, "error");
        });

    //add a new category
    $scope.addCategory = function addCategory() {
        if($scope.categoryName==""){
            swal("Please provide values for category name", "", "info");
        }
        else {
            CategoryService.addCategory({
                id: null,
                name: $scope.categoryName
            }).then(function mySuccess() {
                swal("Category added", "", "success");
                $route.reload();
            }, function myError(response) {
                swal("Error POST category", response, "error");
            });
        }
    };
});
