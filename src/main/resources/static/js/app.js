var app = angular.module('petstoreapp', ['ngRoute','ngTable','ngSanitize', 'ui.select']);

app.config(function($routeProvider){
    $routeProvider
        .when('/pets',{
            templateUrl: '/views/pets.html',
            controller: 'petsController'
        })
        .when('/categories',{
            templateUrl: '/views/categories.html',
            controller: 'categoriesController'
        })
        .when('/tags',{
            templateUrl: '/views/tags.html',
            controller: 'tagsController'
        })
        .otherwise({
            redirectTo:'/pets'
        })
});
