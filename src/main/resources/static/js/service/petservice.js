app.factory('PetService', ['$http', function($http) {

    var petService = {};
    var urlBase="/api/pet";

    petService.getPets = function() {
        return $http.get(urlBase);
    };

    petService.addPet = function(pet) {
        return $http.post(urlBase, pet);
    };

    petService.deletePet = function(id) {
        return $http.delete(urlBase + "/delete/" + id, id);
    };

    return petService;
}]);
