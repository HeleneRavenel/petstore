app.factory('TagService', ['$http', function($http) {

    var tagService = {};
    var urlBase="/api/tag";

    tagService.getTags = function() {
        return $http.get(urlBase);
    };

    tagService.addTag = function(tag) {
        return $http.post(urlBase, tag);
    };

    return tagService;
}]);