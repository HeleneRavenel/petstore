app.factory('CategoryService', ['$http', function($http) {

    var categoryService = {};
    var urlBase="/api/category";

    categoryService.getCategories = function() {
        return $http.get(urlBase);
    };

    categoryService.addCategory = function(category) {
        return $http.post(urlBase, category);
    };

    return categoryService;
}]);