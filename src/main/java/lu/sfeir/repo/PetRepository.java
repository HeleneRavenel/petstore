package lu.sfeir.repo;

import lu.sfeir.entity.PetEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PetRepository extends CrudRepository<PetEntity, Integer> {
}
