package lu.sfeir.repo;

import lu.sfeir.entity.CategoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CategoryRepository extends CrudRepository<CategoryEntity, Integer> {
}
