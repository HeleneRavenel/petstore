package lu.sfeir.repo;

import lu.sfeir.entity.TagEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TagRepository extends CrudRepository<TagEntity, Integer> {
}
