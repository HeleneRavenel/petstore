package lu.sfeir.service;

import lu.sfeir.dto.Tag;
import lu.sfeir.entity.TagEntity;
import lu.sfeir.mapper.TagMapper;
import lu.sfeir.repo.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagService {

    @Autowired
    private TagRepository tagRepo;

    @Autowired
    private TagMapper tagMapper;

    public Tag addTag(Tag tag) {
        TagEntity tagEntity = new TagEntity();
        tagEntity.setTagName(tag.getName());
        tagEntity = tagRepo.save(tagEntity);
        return tagMapper.getTagFromEntity(tagEntity);
    }

    public List<Tag> getAll() {
        List<TagEntity> tagEntities = (List<TagEntity>) tagRepo.findAll();
        List<Tag> tags = new ArrayList<>();
        for (TagEntity t : tagEntities) {
            tags.add(tagMapper.getTagFromEntity(t));
        }
        return tags;
    }

}
