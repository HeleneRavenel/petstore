package lu.sfeir.service;

import lu.sfeir.dto.Pet;
import lu.sfeir.entity.PetEntity;
import lu.sfeir.mapper.PetMapper;
import lu.sfeir.repo.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PetService {

    @Autowired
    private PetRepository petRepo;

    @Autowired
    private PetMapper petMapper;

    public Pet addPet(Pet pet) {
        PetEntity petEntity = petMapper.getEntityFromPet(pet);
        petEntity = petRepo.save(petEntity);
        return petMapper.getPetFromEntity(petEntity);
    }

    public List<Pet> getAll() {
        List<PetEntity> petEntities = (List<PetEntity>) petRepo.findAll();
        List<Pet> pets = new ArrayList<>();
        for (PetEntity p : petEntities) {
            pets.add(petMapper.getPetFromEntity(p));
        }
        return pets;
    }

    public void deletePet(Integer id) {
        petRepo.delete(id);
    }

}
