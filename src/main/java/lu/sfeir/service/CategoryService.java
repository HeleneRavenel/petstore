package lu.sfeir.service;

import lu.sfeir.dto.Category;
import lu.sfeir.entity.CategoryEntity;
import lu.sfeir.mapper.CategoryMapper;
import lu.sfeir.repo.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepo;

    @Autowired
    private CategoryMapper categoryMapper;

    public Category addCategory(Category category) {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setCategoryName(category.getName());
        categoryEntity = categoryRepo.save(categoryEntity);
        return categoryMapper.getCategoryFromEntity(categoryEntity);
    }

    public Category findById(int id) {
        CategoryEntity categoryEntity = categoryRepo.findOne(id);
        return categoryMapper.getCategoryFromEntity(categoryEntity);
    }

    public List<Category> getAll() {
        List<CategoryEntity> categoriesEntities = (List<CategoryEntity>) categoryRepo.findAll();
        List<Category> categories = new ArrayList<>();
        for (CategoryEntity c : categoriesEntities) {
            categories.add(categoryMapper.getCategoryFromEntity(c));
        }
        return categories;
    }
}
