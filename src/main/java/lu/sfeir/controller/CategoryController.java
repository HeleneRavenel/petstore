package lu.sfeir.controller;

import lu.sfeir.dto.Category;
import lu.sfeir.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Category addCategory(@RequestBody Category category) {
        category.setName(category.getName().toUpperCase());
        return categoryService.addCategory(category);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Category getCategory(@PathVariable(value="id") Integer id) {
        return categoryService.findById(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    List<Category> getCategories() {
        return categoryService.getAll();
    }
}
