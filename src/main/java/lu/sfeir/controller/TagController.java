package lu.sfeir.controller;

import lu.sfeir.dto.Tag;
import lu.sfeir.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Tag addTag(@RequestBody Tag tag) {
        tag.setName(tag.getName().toUpperCase());
        return tagService.addTag(tag);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    List<Tag> getTags() {
        return tagService.getAll();
    }
}
