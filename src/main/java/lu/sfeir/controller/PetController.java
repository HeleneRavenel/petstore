package lu.sfeir.controller;

import lu.sfeir.dto.Pet;
import lu.sfeir.service.PetService;
import lu.sfeir.util.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pet")
public class PetController {

    @Autowired
    private PetService petService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Pet addPet(@RequestBody Pet pet) {
        pet.setName(pet.getName().toUpperCase());
        (pet.getTags()).stream().map(tag -> tag.getName().toUpperCase()).collect(Collectors.toList());
        return petService.addPet(pet);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    List<Pet> getPets() {
        return petService.getAll();
    }

    @RequestMapping(value = "/statuses", method = RequestMethod.GET)
    public @ResponseBody
    StatusEnum[] getStatuses() { return StatusEnum.values(); }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void deletePet(@PathVariable(value="id") Integer id) {
        petService.deletePet(id);
    }

}
