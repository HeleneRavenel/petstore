package lu.sfeir.entity;

import javax.persistence.*;

@Entity
@Table(name="PET_TAGS")
public class PetTagsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="pt_id")
    private int ptId;

    @Column(name="pt_pet_id")
    private int ptPetId;

    @Column(name="pt_tag_id")
    private int ptTagId;

    public int getPtId() {
        return ptId;
    }

    public void setPtId(int ptId) {
        this.ptId = ptId;
    }

    public int getPtPetId() {
        return ptPetId;
    }

    public void setPtPetId(int ptPetId) {
        this.ptPetId = ptPetId;
    }

    public int getPtTagId() {
        return ptTagId;
    }

    public void setPtTagId(int ptTagId) {
        this.ptTagId = ptTagId;
    }

    @Override
    public String toString() {
        return "PetTagsEntity{" +
                "ptId=" + ptId +
                ", ptPetId=" + ptPetId +
                ", ptTagId=" + ptTagId +
                '}';
    }
}
