package lu.sfeir.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="TAG")
public class TagEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="tag_id")
    private int tagId;

    @Column(name="tag_name")
    private String tagName;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags")
    private Set<PetEntity> petEntities;

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Set<PetEntity> getPetEntities() {
        return petEntities;
    }

    public void setPetEntities(Set<PetEntity> petEntities) {
        this.petEntities = petEntities;
    }

    @Override
    public String toString() {
        return "TagEntity{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                ", petEntities=" + petEntities +
                '}';
    }
}
