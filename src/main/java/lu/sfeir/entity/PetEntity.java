package lu.sfeir.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="PET")
public class PetEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="pet_id")
    private int petId;

    @ManyToOne
    @JoinColumn(name="pet_category")
    private CategoryEntity petCategory;

    @Column(name="pet_name")
    private String petName;

    @Column(name="pet_status")
    private String petStatus;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "pet_tags", joinColumns = {
            @JoinColumn(name = "PT_PET_ID", nullable = false) },
            inverseJoinColumns = { @JoinColumn(name = "PT_TAG_ID",
                    nullable = false) })
    private Set<TagEntity> tags;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="pet")
    private Set<PetPhotoUrlsEntity> photoUrls;

    public int getPetId() {
        return petId;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public CategoryEntity getPetCategory() {
        return petCategory;
    }

    public void setPetCategory(CategoryEntity petCategory) {
        this.petCategory = petCategory;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetStatus() {
        return petStatus;
    }

    public void setPetStatus(String petStatus) {
        this.petStatus = petStatus;
    }

    public Set<TagEntity> getTags() {
        return tags;
    }

    public void setTags(Set<TagEntity> tags) {
        this.tags = tags;
    }

    public Set<PetPhotoUrlsEntity> getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(Set<PetPhotoUrlsEntity> photoUrls) {
        this.photoUrls = photoUrls;
    }

    @Override
    public String toString() {
        return "PetEntity{" +
                "petId=" + petId +
                ", petCategory=" + petCategory +
                ", petName='" + petName + '\'' +
                ", petStatus='" + petStatus + '\'' +
                ", tags=" + tags +
                ", photoUrls=" + photoUrls +
                '}';
    }
}
