package lu.sfeir.entity;

import javax.persistence.*;

@Entity
@Table(name="PET_PHOTOURLS")
public class PetPhotoUrlsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="pp_id")
    private int ppId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PP_PET_ID", nullable = false)
    private PetEntity pet;

    @Column(name="pp_photourl")
    private String ppPhotoUrl;

    public int getPpId() {
        return ppId;
    }

    public void setPpId(int ppId) {
        this.ppId = ppId;
    }

    public PetEntity getPet() {
        return pet;
    }

    public void setPet(PetEntity pet) {
        this.pet = pet;
    }

    public String getPpPhotoUrl() {
        return ppPhotoUrl;
    }

    public void setPpPhotoUrl(String ppPhotoUrl) {
        this.ppPhotoUrl = ppPhotoUrl;
    }

    @Override
    public String toString() {
        return "PetPhotoUrlsEntity{" +
                "ppId=" + ppId +
                ", pet=" + pet +
                ", ppPhotoUrl='" + ppPhotoUrl + '\'' +
                '}';
    }
}
