package lu.sfeir.mapper;

import lu.sfeir.dto.Category;
import lu.sfeir.entity.CategoryEntity;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

    public CategoryEntity getEntityFromCategory(Category category) {
        CategoryEntity categoryEntity = null;
        if (category != null) {
            categoryEntity = new CategoryEntity();
            categoryEntity.setCategryId(category.getId());
            categoryEntity.setCategoryName(category.getName());
        }
        return categoryEntity;
    }

    public Category getCategoryFromEntity(CategoryEntity categoryEntity) {
        Category category = null;
        if (categoryEntity != null) {
            category = new Category();
            category.setId(categoryEntity.getCategoryId());
            category.setName(categoryEntity.getCategoryName());
        }
        return category;
    }

}
