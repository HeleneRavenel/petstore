package lu.sfeir.mapper;

import lu.sfeir.dto.Tag;
import lu.sfeir.entity.TagEntity;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    public TagEntity getEntityFromTag(Tag tag) {
        TagEntity tagEntity = null;
        if (tag != null) {
            tagEntity = new TagEntity();
            tagEntity.setTagName(tag.getName());
            tagEntity.setTagId(tag.getId());
        }
        return tagEntity;
    }

    public Tag getTagFromEntity(TagEntity tagEntity) {
        Tag tag = null;
        if (tagEntity != null) {
            tag = new Tag();
            tag.setName(tagEntity.getTagName());
            tag.setId(tagEntity.getTagId());
        }
        return tag;
    }

}
