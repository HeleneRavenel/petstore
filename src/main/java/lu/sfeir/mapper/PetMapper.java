package lu.sfeir.mapper;

import lu.sfeir.dto.Category;
import lu.sfeir.dto.Pet;
import lu.sfeir.dto.PetPhotoUrls;
import lu.sfeir.dto.Tag;
import lu.sfeir.entity.PetEntity;
import lu.sfeir.entity.PetPhotoUrlsEntity;
import lu.sfeir.entity.TagEntity;
import lu.sfeir.util.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class PetMapper {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private PetPhotoUrlsMapper petPhotoUrlsMapper;

    public PetEntity getEntityFromPet(Pet pet) {
        PetEntity petEntity = null;
        if (pet != null) {
            petEntity = new PetEntity();
            petEntity.setPetId(pet.getId());
            petEntity.setPetCategory(categoryMapper.getEntityFromCategory(pet.getCategory()));
            petEntity.setPetName(pet.getName());
            petEntity.setPetStatus(pet.getStatus().getStatus());
            if (pet.getTags() != null) {
                Set<TagEntity> tags = new HashSet<>();
                for (Tag tag : pet.getTags()) {
                    tags.add(tagMapper.getEntityFromTag(tag));
                }
                petEntity.setTags(tags);
            }
            if (pet.getPhotoUrls() != null) {
                Set<PetPhotoUrlsEntity> photoUrls = new HashSet<>();
                for (PetPhotoUrls photo : pet.getPhotoUrls()) {
                    PetPhotoUrlsEntity petPhotoUrlsEntity = petPhotoUrlsMapper.getEntityFromPetPhotoUrls(photo);
                    petPhotoUrlsEntity.setPet(petEntity);
                    photoUrls.add(petPhotoUrlsEntity);
                }
                petEntity.setPhotoUrls(photoUrls);
            }
        }
        return petEntity;
    }

    public Pet getPetFromEntity(PetEntity petEntity) {
        Pet pet = null;
        if (petEntity != null) {
            pet = new Pet();
            pet.setId(petEntity.getPetId());
            Category category = categoryMapper.getCategoryFromEntity(petEntity.getPetCategory());
            //Category category = categoryService.findById(petEntity.getPetCategory());
            pet.setCategory(category);
            pet.setName(petEntity.getPetName());
            pet.setStatus(StatusEnum.fromStatus(petEntity.getPetStatus()));
            if (petEntity.getTags() != null) {
                List<Tag> tags = new ArrayList<>();
                for (TagEntity te : petEntity.getTags()) {
                    tags.add(tagMapper.getTagFromEntity(te));
                }
                pet.setTags(tags);
            }
            if (petEntity.getPhotoUrls() != null) {
                List<PetPhotoUrls> petPhotoUrlses = new ArrayList<>();
                for (PetPhotoUrlsEntity ppue : petEntity.getPhotoUrls()) {
                    petPhotoUrlses.add(petPhotoUrlsMapper.getPhotoUrlsFromEntity(ppue));
                }
                pet.setPhotoUrls(petPhotoUrlses);
            }

        }
        return pet;
    }

}
