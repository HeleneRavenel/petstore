package lu.sfeir.mapper;

import lu.sfeir.dto.PetPhotoUrls;
import lu.sfeir.entity.PetPhotoUrlsEntity;
import org.springframework.stereotype.Component;

@Component
public class PetPhotoUrlsMapper {

    public PetPhotoUrlsEntity getEntityFromPetPhotoUrls(PetPhotoUrls petPhotoUrls) {
        PetPhotoUrlsEntity petPhotoUrlsEntity = null;
        if (petPhotoUrls != null) {
            petPhotoUrlsEntity = new PetPhotoUrlsEntity();
            petPhotoUrlsEntity.setPpPhotoUrl(petPhotoUrls.getPhotoUrl());
            petPhotoUrlsEntity.setPpId(petPhotoUrls.getPpId());
        }
        return petPhotoUrlsEntity;
    }

    public PetPhotoUrls getPhotoUrlsFromEntity(PetPhotoUrlsEntity ppue) {
        PetPhotoUrls petPhotoUrls = null;
        if (ppue != null) {
            petPhotoUrls = new PetPhotoUrls();
            petPhotoUrls.setPhotoUrl(ppue.getPpPhotoUrl());
            petPhotoUrls.setPpId(ppue.getPpId());
        }
        return petPhotoUrls;
    }
}
