package lu.sfeir.util;

public enum StatusEnum {

    AVAILABLE("Available"),
    PENDING("Pending"),
    SOLD("Sold");

    private String status;

    StatusEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static StatusEnum fromStatus(String status) {
        StatusEnum ret = null;
        for (StatusEnum se : values()) {
            if (se.getStatus().equals(status)) {
                ret = se;
                break;
            }
        }
        return ret;
    }
}
