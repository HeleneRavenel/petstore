package lu.sfeir.dto;

public class PetPhotoUrls {

    private int ppId;
    private String photoUrl;

    public PetPhotoUrls() {
    }

    public int getPpId() {
        return ppId;
    }

    public void setPpId(int ppId) {
        this.ppId = ppId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}
