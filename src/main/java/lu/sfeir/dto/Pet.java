package lu.sfeir.dto;

import lu.sfeir.util.StatusEnum;

import java.util.List;

public class Pet {

    private int id;
    private Category category;
    private String name;
    private List<PetPhotoUrls> photoUrls;
    private List<Tag> tags;
    private StatusEnum status;

    public Pet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCategory() { return category; }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PetPhotoUrls> getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(List<PetPhotoUrls> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}
